from flask import Flask, request, abort, render_template, redirect, url_for
app = Flask(__name__)

import json
import os.path

try:
    with open('database.json', 'r') as f:
        status = json.load(f).status
except:
    with open('database.json', 'w') as f:
        json.dump({'status': 'initialized'}, f)

def redirect_to_main(request_data):
    return redirect(url_for('main_page',
        branches = request_data['branches'],
        release_version = request_data['release_version'],
        snapshot_version = request_data['snapshot_version']))

@app.route('/')
def redirect_to_main_page():
    return redirect(url_for('main_page'))

@app.route('/begin')
def main_page():
    with open('database.json', 'r') as f:
        current_status = json.load(f).get('status')
    return render_template('frontend.html',
        current_status = current_status,
        branches = request.args.get('branches'),
        release_version = request.args.get('release_version'),
        snapshot_version = request.args.get('snapshot_version'))

@app.route('/start', methods = ['POST'])
def start():
    request_data = {
        'branches' : request.form['branches'],
        'release_version' : request.form['release_version'],
        'snapshot_version' : request.form['snapshot_version']
    }

    if not request_data['branches'] or not request_data['release_version'] or not request_data['snapshot_version'] :
        abort(400, 'For GOD SAKE')

    with open('database.json', 'r') as f:
        data = json.load(f)

    if not data.get('status') == 'started' and not data.get('status').startswith('processing') :
        text = {
            'status' : 'started',
            'branches' : request_data['branches'].split(),
            'release_version' : request_data['release_version'],
            'snapshot_version' : request_data['snapshot_version']
        }

        with open('database.json', 'w') as j_f:
            json.dump(text, j_f)

    return redirect_to_main(request_data)

@app.route('/abort', methods = ['POST'])
def re_init_file():
    request_data = {
        'branches' : request.form['branches'],
        'release_version' : request.form['release_version'],
        'snapshot_version' : request.form['snapshot_version']
    }
    with open('database.json', 'w') as j_f:
        json.dump({'status': 'initialized'}, j_f)
    return redirect_to_main(request_data)

if __name__ == '__main__':
    from waitress import serve
    serve(app, host='0.0.0.0', port=5000)