import time
import json
import subprocess
import argparse
import sys
import os

parser = argparse.ArgumentParser()
parser.add_argument('--build-dir', help='build directory')
args = parser.parse_args()
if not args.build_dir:
    raise Exception('please set --build-dir option')

BUILD_DIRECTORY = args.build_dir
GIT_DIRECTORY = '-C {}'.format(BUILD_DIRECTORY)
MVN_DIRECTORY = '-f {}/bochka-project/pom.xml'.format(BUILD_DIRECTORY)

try:
    with open('database.json', 'r') as f:
        status = json.load(f).status
except:
    with open('database.json', 'w') as f:
        json.dump({'status': 'initialized'}, f)

with open('inventory.json') as j_file:
    inventory = json.load(j_file)

with open('credentials.json') as json_file:
    credentials = json.load(json_file)


def execute_shell_script(stage_name, script):

    with open('database.json', 'w') as j_f:
        json.dump({'status': 'processing - {}'.format(stage_name)}, j_f)

    subprocess.check_output('mkdir -p logs', shell=True)
    subprocess.check_output('touch logs/{0}'.format(stage_name), shell=True)
    subprocess.check_output('{0} &>logs/{1}'.format(script, stage_name), shell=True)

def execute_nohup_shell_script(stage_name, script):
    with open('database.json', 'w') as j_f:
        json.dump({'status': 'processing - {}'.format(stage_name)}, j_f)

    subprocess.check_output('mkdir -p logs', shell=True)
    subprocess.check_output('touch logs/{0}'.format(stage_name), shell=True)
    subprocess.Popen('{0} &>logs/{1}'.format(script, stage_name), shell=True)

while True:
    time.sleep(3)

    with open('database.json', 'r') as f:
        data = json.load(f)

    if data.get('status') == 'started' :
        with open('database.json', 'w') as j_f:
            json.dump({'status': 'processing - begin'}, j_f)

        execute_shell_script('clear_workspace', 'rm -rf {}'.format(BUILD_DIRECTORY))

        execute_shell_script('git_clone','git clone https://landauleo@bitbucket.org/landauleo/bochka.git {}'.format(BUILD_DIRECTORY))

        execute_shell_script('release_candidate_checkout', 'git {} checkout release-candidate '.format(GIT_DIRECTORY))

        for branch in data.get('branches') :
            execute_shell_script('merge_branch_{}_to_RC'.format(branch), 'git {} merge --no-ff origin/{}'.format(GIT_DIRECTORY, branch))

        execute_shell_script('mvn_release_prepare', 'mvn {0} -Dtag=bochka-project-{1} release:prepare -Dusername={3} -Dpassword={4} -DreleaseVersion={1} -DdevelopmentVersion={2}'.format(MVN_DIRECTORY, data.get('release_version'), data.get('snapshot_version'), credentials.get('username'), credentials.get('password')))

        execute_shell_script('checkout_to_release_tag', 'git {} checkout bochka-project-{}'.format(GIT_DIRECTORY, data.get('release_version')))

        execute_shell_script('mvn_clean_install', 'mvn {} clean install'.format(MVN_DIRECTORY))

        for host in inventory.get('hosts') :

            execute_shell_script('copy_back_office_jar_to_home','sshpass -p \'{0}\' scp {1}/bochka-project/back-office/target/back-office-{2}.jar {3}@{4}:/home/{3}'.format(credentials.get('deployerPassword'), format(BUILD_DIRECTORY), data.get('release_version') , credentials.get('deployer'), host))

            execute_shell_script('copy_client_jar_to_home','sshpass -p \'{0}\' scp {1}/bochka-project/client/target/client-{2}.jar {3}@{4}:/home/{3}'.format(credentials.get('deployerPassword'), format(BUILD_DIRECTORY), data.get('release_version'), credentials.get('deployer'), host))

            execute_shell_script('move_back_office_jar_to_opt','echo \'{0}\' | sshpass -p \'{0}\' ssh {1}@{2} \'sudo -S mv /home/{1}/back-office-{3}.jar /opt/bochka-back-office/back-office\''.format(credentials.get('deployerPassword'), credentials.get('deployer'), host, data.get('release_version')))

            execute_shell_script('move_client_jar_to_opt','echo \'{0}\' | sshpass -p \'{0}\' ssh {1}@{2} \'sudo -S mv /home/{1}/client-{3}.jar /opt/bochka-client/client\''.format(credentials.get('deployerPassword'), credentials.get('deployer'), host, data.get('release_version')))

            execute_nohup_shell_script('start_back-office','echo \'{0}\' | sshpass -p \'{0}\' ssh {1}@{2} \'sudo -S -u {3} nohup java -jar /opt/bochka-back-office/back-office/back-office-{4}.jar --spring.config.additional-location=/opt/bochka-back-office/back-office/bochka-back-office.properties\' > /dev/null 2>&1 &'.format(credentials.get('deployerPassword'), credentials.get('deployer'), host, credentials.get("starter"), data.get('release_version')))

            execute_nohup_shell_script('start_client','echo \'{0}\' | sshpass -p \'{0}\' ssh {1}@{2} \'sudo -S -u {3} nohup java -jar /opt/bochka-client/client/client-{4}.jar --spring.config.additional-location=/opt/bochka-client/client/bochka-client.properties\' > /dev/null 2>&1 &'.format(credentials.get('deployerPassword'), credentials.get('deployer'), host, credentials.get("starter"), data.get('release_version')))

        with open('database.json', 'w') as j_f:
            json.dump({'status': 'finished'}, j_f)
